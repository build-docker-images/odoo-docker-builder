### setup a build server

ip=$(knife digital_ocean droplet create -N builder -I ubuntu-14-04-x64 -S 1gb -L sgp1 -K 1665513)

ssh root@"$ip"

apt-get update && apt-get install wget git tree docker.io -y
cd /tmp
wget http://nightly.odoo.com/8.0/nightly/deb/odoo_8.0.20160324_all.deb
wget -O /tmp/wkhtmltox-0.12.2.1_linux-trusty-amd64.deb http://download.gna.org/wkhtmltopdf/0.12/0.12.1/wkhtmltox-0.12.1_linux-trusty-amd64.deb
git clone https://github.com/dfang/odoo.git


knife digital_ocean droplet power -I 14203294 -a off
knife digital_ocean droplet snapshot -N initial-snapshot -I 14203294
knife digital_ocean droplet power -I 14203294 -a on

docker pull ubuntu:14.04
docker pull postgres

### upload and build image

rsync -avP ~/Projects/source_code/dfang/build_odoo_by_docker/build_on_remote odoo:
ssh odoo
cd ~/build_on_remote
./build.sh


### run 

docker run -d -e POSTGRES_USER=odoo -e POSTGRES_PASSWORD=odoo --name db postgres:9.3
docker run -p 8069:8069 --name odoo --link db:db -d ubuntu1404-odoo8

docker exec -it odoo /bin/bash
docker logs -f odoo