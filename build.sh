#! /bin/sh

# https://github.com/aschenkels-ictstudio/odoo-install-scripts/tree/9.0/ubuntu-14-04

# ssh odoo

cd ~/build_on_remote
# docker build -t dfang/intermediate -f ubuntu-14-04/intermediate/Dockerfile .

docker build -t ubuntu1404-odoo8 -f ubuntu-14-04/8.0/Dockerfile .
docker build -t ubuntu1404-odoo9 -f ubuntu-14-04/9.0/Dockerfile .
docker build -t ubuntu1404-saas9 -f ubuntu-14-04/saas-9/Dockerfile .
docker build -t ubuntu1404-saas10 -f ubuntu-14-04/saas-10/Dockerfile .


# docker ps -aq | xargs docker rm -f
# docker run -d -e POSTGRES_USER=odoo -e POSTGRES_PASSWORD=odoo --name db postgres
# docker run -p 8069:8069 --name odoo --link db:db -t --rm ubuntu1404-odoo8
# /bin/bash


# docker images -a | grep -v postgres | grep -v ubuntu


# docker exec -it dfang/intermediate /bin/bash
# docker run -it dfang/intermediate /bin/bash