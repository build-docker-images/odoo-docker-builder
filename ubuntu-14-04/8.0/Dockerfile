FROM ubuntu:14.04
MAINTAINER df1228@gmail.com

ENV OE_USER="odoo"
ENV OE_HOME="/opt/${OE_USER}"

# Enter version for checkout "8.0" for version 8.0, "7.0 (version 7), saas-4, saas-5 (opendays version) and "master" for trunk
ENV OE_VERSION="8.0"

# Create odoo system user and enable password-less sudo
RUN adduser --system --quiet --shell=/bin/bash --home=${OE_HOME} --gecos 'ODOO' --group ${OE_USER}
RUN echo "%odoo ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# for better caching when build image
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install curl wget git python-setuptools python-pip -y

RUN wget -O /tmp/wkhtmltox-0.12.2.1_linux-trusty-amd64.deb http://download.gna.org/wkhtmltopdf/0.12/0.12.1/wkhtmltox-0.12.1_linux-trusty-amd64.deb


# The NodeJS and NPM packages in the Ubuntu 14.04 repository can not be used because they are outdated.
# http://stackoverflow.com/questions/29443838/after-odoo-installation-either-css-nor-js-are-found
# https://www.odoo.com/documentation/8.0/setup/install.html#setup-install-source
RUN curl -sL https://deb.nodesource.com/setup_0.10 | bash - \
      && apt-get install -y nodejs \
      && npm install -g npm \
      && npm install -g less less-plugin-clean-css

# # install python 2.7.9 on ubuntu 14.04, use python -V to check, https://gist.github.com/dfang/c463f3420a801480b1c8041372889a1f
# RUN apt-get install -y build-essential checkinstall autoconf automake python-pip libbz2-dev \
#         libc6-dev \
#         libgdbm-dev \
#         libncursesw5-dev \
#         libreadline-gplv2-dev \
#         libsqlite3-dev \
#         libssl-dev \
#         tk-dev
# RUN wget -O /tmp/Python-2.7.9.tgz https://www.python.org/ftp/python/2.7.9/Python-2.7.9.tgz
# RUN cd /tmp && tar xfz /tmp/Python-2.7.9.tgz
# RUN cd /tmp/Python-2.7.9/ && CXX=g++ ./configure --prefix /usr/local/lib/python2.7.9 --enable-ipv6 && make && checkinstall
# RUN ln -sf /usr/local/lib/python2.7.9/bin/python /usr/bin/python
# RUN rm -rf /tmp/Python-2.7.9/
# RUN rm /tmp/Python-2.7.9.tgz


# # install setuptools
# RUN curl https://bootstrap.pypa.io/ez_setup.py -o - | /usr/local/lib/python2.7.9/bin/python
# # use setuptools to install pip
# RUN /usr/local/lib/python2.7.9/bin/easy_install -s /usr/local/lib/python2.7.9/bin -d /usr/local/lib/python2.7/site-packages/ pip
# # update PATH to include the new version of python first
# RUN export PATH="usr/local/lib/python2.7.9/bin:$PATH"

# Install python 2.7.11
RUN apt-get install -y software-properties-common python-software-properties \
      && add-apt-repository -y ppa:fkrull/deadsnakes-python2.7 \
      && apt-get update && apt-get install -y python2.7


# Install Dependencies
# RUN apt-get install -y python-pip
RUN apt-get install -y --no-install-recommends wget git python-dateutil python-feedparser  \
                    python-ldap python-libxslt1 python-lxml python-mako  \
                    python-openid python-psycopg2 python-pybabel  \
                    python-pychart python-pydot python-pyparsing  \
                    python-reportlab python-simplejson python-tz python-vatnumber  \
                    python-vobject python-webdav python-werkzeug python-xlwt  \
                    python-yaml python-zsi python-docutils python-psutil python-mock  \
                    python-unittest2 python-jinja2 python-pypdf \
                    python-decorator python-requests python-passlib \
                    python-imaging python-setuptools python-dev python-support \
                    postgresql-client python-pyinotify python-renderpm \
                    libpq-dev libxslt-dev libxml2-dev libldap2-dev libsasl2-dev 

RUN echo -e "\n---- Install python libraries ----"
RUN pip install gdata

RUN echo -e "\n---- Install unmet dependencies ----"
RUN apt-get -y install -f --no-install-recommends

RUN echo -e "\n---- Install wkhtml and place on correct place for ODOO 8 ----"
################################### TODO
# ADD wkhtmltox-0.12.2.1_linux-trusty-amd64.deb /tmp/wkhtmltox-0.12.2.1_linux-trusty-amd64.deb
RUN dpkg -i --force-depends /tmp/wkhtmltox-0.12.2.1_linux-trusty-amd64.deb
RUN apt-get -y install -f --no-install-recommends
RUN cp /usr/local/bin/wkhtmltopdf /usr/bin && cp /usr/local/bin/wkhtmltoimage /usr/bin

# #--------------------------------------------------
# # Install ODOO
# #--------------------------------------------------
RUN echo -e "\n==== Installing ODOO Server ===="
RUN git clone --depth 1 --single-branch --branch $OE_VERSION https://www.github.com/dfang/odoo ${OE_HOME}/
################################### TODO
# RUN mkdir -p /tmp/odoo
# ADD odoo /tmp/odoo
# RUN su ${OE_USER} -c "git clone --depth 1 --single-branch --branch $OE_VERSION /tmp/odoo ${OE_HOME}/"
# RUN git clone --depth 1 --single-branch --branch $OE_VERSION https://www.github.com/dfang/odoo ${OE_HOME}/

RUN echo -e "\n---- Setting permissions on home folder ----"
RUN chown -R ${OE_USER}:${OE_USER} ${OE_HOME}/*

#--------------------------------------------------
# Install Dependencies
#--------------------------------------------------
RUN echo -e "\n---- Install tool packages ----"
RUN pip install -r ${OE_HOME}/requirements.txt

RUN echo -e "\n---- Install python packages ----"
RUN easy_install pyPdf vatnumber pydot psycogreen suds ofxparse

# Do some clean work
RUN rm -rf /tmp/* && apt-get autoremove && apt-get clean && apt-get autoclean 

# Copy entrypoint script and Odoo configuration file
COPY ubuntu-14-04/8.0/entrypoint.sh /
COPY ubuntu-14-04/8.0/openerp-server.conf /etc/odoo/

RUN chown odoo:odoo /entrypoint.sh \
    && chmod +x /entrypoint.sh \
    && chown odoo:odoo /etc/odoo/openerp-server.conf

# Mount /var/lib/odoo to allow restoring filestore and /mnt/extra-addons for users addons
RUN mkdir -p /mnt/extra-addons \
      && mkdir -p /var/lib/odoo \
      && chown -R odoo:odoo /mnt/extra-addons \
      && chown -R odoo:odoo /var/lib/odoo

VOLUME ["/var/lib/odoo", "/mnt/extra-addons"]

# Expose Odoo services
EXPOSE 8069 8071

# Set the default config file
ENV OPENERP_SERVER /etc/odoo/openerp-server.conf

# Set default user when running the container
USER odoo

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/opt/odoo/openerp-server"]
